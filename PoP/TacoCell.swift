//
//  TacoCell.swift
//  PoP
//
//  Created by Pragun Sharma on 24/07/17.
//  Copyright © 2017 Pragun Sharma. All rights reserved.
//

import UIKit

class TacoCell: UICollectionViewCell, NibLoadableView, ReusableView, Shakeable {

    
    @IBOutlet weak var tacoImage: UIImageView!
    @IBOutlet weak var tacoLabel: UILabel!
    
    var taco: Taco!
    
    func configureCell(taco: Taco) {
        self.taco = taco
        tacoImage.image = UIImage(named: taco.proteinId.rawValue)
        tacoLabel.text = taco.productName
    }
}
