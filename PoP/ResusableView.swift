//
//  ResusableView.swift
//  PoP
//
//  Created by Pragun Sharma on 24/07/17.
//  Copyright © 2017 Pragun Sharma. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    static var resuseIdentifier: String {
        return String(describing: self)
    }
}
