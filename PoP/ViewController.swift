//
//  ViewController.swift
//  PoP
//
//  Created by Pragun Sharma on 24/07/17.
//  Copyright © 2017 Pragun Sharma. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DataServiceDelegate {

    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var dataServ: DataService = DataService.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.addDropShadow()
        
        dataServ.delegate = self
        dataServ.loadDeliciousTacoData()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(TacoCell.self)

    }

    
    func deliciousTacoDataloaded() {
        print("Reached")
        collectionView.reloadData()
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataServ.tacoArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        /*
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TacoCell", for: indexPath) as? TacoCell {
            cell.configureCell(taco: dataServ.tacoArray[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
        */
        
        //New Way of Protocols
        let cell = collectionView.dequeueReusableCell(forIndexpath: indexPath) as TacoCell
        cell.configureCell(taco: dataServ.tacoArray[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? TacoCell {
            cell.shake()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 95)
    }
}
