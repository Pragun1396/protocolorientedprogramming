//
//  NibLoadableView.swift
//  PoP
//
//  Created by Pragun Sharma on 24/07/17.
//  Copyright © 2017 Pragun Sharma. All rights reserved.
//

import Foundation
import UIKit

protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}
