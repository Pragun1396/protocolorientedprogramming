//
//  UICollectionView+Ext.swift
//  PoP
//
//  Created by Pragun Sharma on 24/07/17.
//  Copyright © 2017 Pragun Sharma. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.resuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexpath indexPath: IndexPath) -> T where T: ReusableView {
        
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.resuseIdentifier, for: indexPath as IndexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.resuseIdentifier)")
        }
        return cell
    }
}

